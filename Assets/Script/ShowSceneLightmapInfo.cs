﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ShowSceneLightmapInfo : MonoBehaviour
{

    public Renderer[] renderers;

    void Start()
    {
        List<string> messages = new List<string>();

        messages.Add("");
        messages.Add("lightmapsMode");
        messages.Add(LightmapSettings.lightmapsMode.ToString());
        messages.Add("lightmaps.Length");
        messages.Add(LightmapSettings.lightmaps.Length.ToString());

        //string fileName = "";
        //byte[] bytes;
        //Texture2D texture = null;
        for (int i = 0; i < LightmapSettings.lightmaps.Length; i++)
        {
            //fileName = string.Format("{0}/Lightmap/{1}{2}.png", Application.streamingAssetsPath, "Color", i);
            //texture = CopyTexute(LightmapSettings.lightmaps[i].lightmapColor);
            //bytes = texture.EncodeToPNG();
            ////不支持直接导出图片
            ////bytes = LightmapSettings.lightmaps[i].lightmapColor.EncodeToPNG();
            //if (null != bytes)
            //{
            //    File.WriteAllBytes(fileName, bytes);
            //}

            //fileName = string.Format("{0}/Lightmap/{1}{2}.png", Application.streamingAssetsPath, "Dir", i);
            //texture = CopyTexute(LightmapSettings.lightmaps[i].lightmapDir);
            //bytes = texture.EncodeToPNG();
            ////不支持直接导出图片
            ////bytes = LightmapSettings.lightmaps[i].lightmapDir.EncodeToPNG();
            //if (null != bytes)
            //{
            //    File.WriteAllBytes(fileName, bytes);
            //}

            messages.Add(string.Format("Color{0}.name = {1}", i, LightmapSettings.lightmaps[i].lightmapColor.name));
            messages.Add(string.Format("Dir{0}.name = {1}", i, LightmapSettings.lightmaps[i].lightmapDir.name));
        }
        
        messages.Add("-------------");

        //Show Renderers
        foreach(Renderer item in renderers)
        {
            messages.Add(item.gameObject.name);
            messages.Add(string.Format("{0}={1}", "lightmapIndex", item.lightmapIndex));
            messages.Add(string.Format("{0}={1}", "lightmapScaleOffset", item.lightmapScaleOffset));
        }

        string message = "";
        foreach (string item in messages)
        {
            message = message + item + "\n";
        }

        Debug.Log(message);
    }

    /// <summary>
    /// 需要将在Inspector面板，将所有光照贴图的"Read/Write Enabled"勾选
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    private Texture2D CopyTexute(Texture2D source)
    {
        int width = source.width;
        int height = source.height;

        Texture2D dest = new Texture2D(width, height);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                dest.SetPixel(x, y, source.GetPixel(x, y));
            }
        }

        return dest;
    }
}
