﻿using System;
using UnityEngine;

public class LoadLightingData : MonoBehaviour
{
    public SceneLightmapInfo SceneLightmapInfo;

    private void Awake()
    {
        //Set LightmapData
        //Set Renderer lightmap info
        LoadLightmapDatas();
        LoadRendererInfos();
    }

    private void LoadRendererInfos()
    {
        GameObject target = null;
        Renderer r = null;
        foreach (LightmapInfo info in SceneLightmapInfo.lightmapInfos)
        {
            target = GameObject.Find(info.gameObjectPath);
            if (null == target)
            {
                continue;
            }

            r = target.GetComponent<Renderer>();
            if (null == r)
            {
                continue;
            }

            r.lightmapIndex = info.lightmapIndex;
            r.lightmapScaleOffset = info.lightmapScaleOffset;
        }
    }

    private void LoadLightmapDatas()
    {
        int length = SceneLightmapInfo.lightmapColorTextures.Length;

        LightmapData[] datas = new LightmapData[length];
        for (int i = 0; i < length; i++)
        {
            datas[i] = new LightmapData();
            datas[i].lightmapColor = SceneLightmapInfo.lightmapColorTextures[i];
            datas[i].lightmapDir = SceneLightmapInfo.lightmapDirTextures[i];
        }
        LightmapSettings.lightmaps = datas;
    }
}
