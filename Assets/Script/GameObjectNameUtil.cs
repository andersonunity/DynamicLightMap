﻿using UnityEngine;

public class GameObjectNameUtil
{
    public static string GetGameObjectFullPath(GameObject go)
    {
        Transform t = go.transform;
        string fullPath = "";

        do
        {
            if ("".Equals(fullPath))
            {
                fullPath = t.gameObject.name;
            }
            else
            {
                fullPath = t.gameObject.name + "/" + fullPath;
            }

            t = t.parent;
        } while (t != null);

        return fullPath;
    }
}
