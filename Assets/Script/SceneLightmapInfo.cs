﻿using System;
using UnityEngine;

[Serializable]
public class LightmapInfo
{
    public string gameObjectPath;
    public int lightmapIndex;
    public Vector4 lightmapScaleOffset;
}

public class SceneLightmapInfo : ScriptableObject
{
    //Lightmap Color and Dir Texture
    public Texture2D[] lightmapColorTextures;
    public Texture2D[] lightmapDirTextures;

    //Renderer LightmapInfo
    public LightmapInfo[] lightmapInfos;
}
